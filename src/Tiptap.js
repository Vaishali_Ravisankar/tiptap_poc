import React from 'react'
import { useEditor, EditorContent } from '@tiptap/react'
import StarterKit from '@tiptap/starter-kit'
import Table from '@tiptap/extension-table'
import TableRow from '@tiptap/extension-table-row'
import TableCell from '@tiptap/extension-table-cell'
import TableHeader from '@tiptap/extension-table-header'
import Document from '@tiptap/extension-document'
import Paragraph from '@tiptap/extension-paragraph'
import Text from '@tiptap/extension-text'
import Image from '@tiptap/extension-image'
import Dropcursor from '@tiptap/extension-dropcursor'
import { Extension } from '@tiptap/core'
import TextAlign from '@tiptap/extension-text-align'
import Highlight from '@tiptap/extension-highlight'
import TextStyle from '@tiptap/extension-text-style'
import FontFamily from '@tiptap/extension-font-family'
import Collaboration from '@tiptap/extension-collaboration'
import * as Y from 'yjs'
import { WebrtcProvider } from 'y-webrtc'




import './styles.css'

const ydoc = new Y.Doc()
// Registered with a WebRTC provider
const provider = new WebrtcProvider('example-document', ydoc)


const addImage = ({editor}) => {
  const url = window.prompt('URL')

  if (url) {
    editor.chain().focus().setImage({ src: url }).run()
  }
}




const CustomTableCell = TableCell.extend({
  addAttributes() {
    return {
      // extend the existing attributes …
      ...this.parent?.(),

      // and add a new one …
      backgroundColor: {
        default: null,
        parseHTML: element => {
          return {
            backgroundColor: element.getAttribute('data-background-color'),
          }
        },
        renderHTML: attributes => {
          return {
            'data-background-color': attributes.backgroundColor,
            style: `background-color: ${attributes.backgroundColor}`,
          }
        },
      },
    }
  },
})

const CustomExtension = Extension.create({
  
  onUpdate({ editor }) {
    // The content has changed.
    console.log(editor.getHTML())
    editor.commands.setContent(editor.getHTML())
  }
})

const MenuBar = ({ editor }) => {
  if (!editor) {
    return null
  }

  return (
    <>
    <div className="MenuContainer">
      <button
        onClick={() => editor.chain().focus().toggleBold().run()}
        className={editor.isActive('bold') ? 'is-active' : ''}
      >
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M8 11h4.5a2.5 2.5 0 1 0 0-5H8v5zm10 4.5a4.5 4.5 0 0 1-4.5 4.5H6V4h6.5a4.5 4.5 0 0 1 3.256 7.606A4.498 4.498 0 0 1 18 15.5zM8 13v5h5.5a2.5 2.5 0 1 0 0-5H8z"/></svg>
      </button>
      <button
        onClick={() => editor.chain().focus().toggleItalic().run()}
        className={editor.isActive('italic') ? 'is-active' : ''}
      >
        I
      </button>
      <button
        onClick={() => editor.chain().focus().toggleStrike().run()}
        className={editor.isActive('strike') ? 'is-active' : ''}
      >
        <s>S</s>
      </button>
      <button
        onClick={() => editor.chain().focus().toggleCode().run()}
        className={editor.isActive('code') ? 'is-active' : ''}
      >
        {"</>"}
      </button>
      {/* <button onClick={() => editor.chain().focus().unsetAllMarks().run()}>
        clear marks
      </button>
      <button onClick={() => editor.chain().focus().clearNodes().run()}>
        clear nodes
      </button> */}
      <button
        onClick={() => editor.chain().focus().setParagraph().run()}
        className={editor.isActive('paragraph') ? 'is-active' : ''}
      >
        P
      </button>
      <button
        onClick={() => editor.chain().focus().toggleHeading({ level: 1 }).run()}
        className={editor.isActive('heading', { level: 1 }) ? 'is-active' : ''}
      >
        h1
      </button>
      <button
        onClick={() => editor.chain().focus().toggleHeading({ level: 2 }).run()}
        className={editor.isActive('heading', { level: 2 }) ? 'is-active' : ''}
      >
        h2
      </button>
      <button
        onClick={() => editor.chain().focus().toggleHeading({ level: 3 }).run()}
        className={editor.isActive('heading', { level: 3 }) ? 'is-active' : ''}
      >
        h3
      </button>
      <button
        onClick={() => editor.chain().focus().toggleHeading({ level: 4 }).run()}
        className={editor.isActive('heading', { level: 4 }) ? 'is-active' : ''}
      >
        h4
      </button>
      <button
        onClick={() => editor.chain().focus().toggleHeading({ level: 5 }).run()}
        className={editor.isActive('heading', { level: 5 }) ? 'is-active' : ''}
      >
        h5
      </button>
      <button
        onClick={() => editor.chain().focus().toggleHeading({ level: 6 }).run()}
        className={editor.isActive('heading', { level: 6 }) ? 'is-active' : ''}
      >
        h6
      </button>
      <button
        onClick={() => editor.chain().focus().toggleBulletList().run()}
        className={editor.isActive('bulletList') ? 'is-active' : ''}
      >
        UL
      </button>
      <button
        onClick={() => editor.chain().focus().toggleOrderedList().run()}
        className={editor.isActive('orderedList') ? 'is-active' : ''}
      >
        OL
      </button>
      <button
        onClick={() => editor.chain().focus().toggleCodeBlock().run()}
        className={editor.isActive('codeBlock') ? 'is-active' : ''}
      >
        code block
      </button>
      <button
        onClick={() => editor.chain().focus().toggleBlockquote().run()}
        className={editor.isActive('blockquote') ? 'is-active' : ''}
      >
        ""
      </button>
      <button onClick={() => editor.chain().focus().setHorizontalRule().run()}>
        Line
      </button>
      <button onClick={() => editor.chain().focus().setHardBreak().run()}>
        Line break
      </button>
      <button onClick={() => editor.chain().focus().undo().run()}>
        undo
      </button>
      <button onClick={() => editor.chain().focus().redo().run()}>
        redo
      </button>
      <button onClick={() => editor.chain().focus().toggleHighlight().run()} className={editor.isActive('highlight') ? 'is-active' : ''}>
        highlight
      </button>
      <button onClick={() => editor.chain().focus().setTextAlign('left').run()} className={editor.isActive({ textAlign: 'left' }) ? 'is-active' : ''}>
        left
      </button>
      <button onClick={() => editor.chain().focus().setTextAlign('center').run()} className={editor.isActive({ textAlign: 'center' }) ? 'is-active' : ''}>
        center
      </button>
      <button onClick={() => editor.chain().focus().setTextAlign('right').run()} className={editor.isActive({ textAlign: 'right' }) ? 'is-active' : ''}>
        right
      </button>
      <button onClick={() => editor.chain().focus().setTextAlign('justify').run()} className={editor.isActive({ textAlign: 'justify' }) ? 'is-active' : ''}>
        justify
      </button>
      <button onClick={addImage}>
        add image from URL
      </button>
      <button onClick={() => editor.chain().focus().insertTable({ rows: 3, cols: 3, withHeaderRow: true }).run()}>
        insertTable
      </button>
      <br/>
     
      <br/>
      <button onClick={() => editor.chain().focus().addColumnBefore().run()} disabled={!editor.can().addColumnBefore()}>
        addColumnBefore
      </button>
      <button onClick={() => editor.chain().focus().addColumnAfter().run()} disabled={!editor.can().addColumnAfter()}>
        addColumnAfter
      </button>
      <button onClick={() => editor.chain().focus().deleteColumn().run()} disabled={!editor.can().deleteColumn()}>
        deleteColumn
      </button>
      <button onClick={() => editor.chain().focus().addRowBefore().run()} disabled={!editor.can().addRowBefore()}>
        addRowBefore
      </button>
      <button onClick={() => editor.chain().focus().addRowAfter().run()} disabled={!editor.can().addRowAfter()}>
        addRowAfter
      </button>
      <button onClick={() => editor.chain().focus().deleteRow().run()} disabled={!editor.can().deleteRow()}>
        deleteRow
      </button>
      <button onClick={() => editor.chain().focus().deleteTable().run()} disabled={!editor.can().deleteTable()}>
        deleteTable
      </button>
      {/* <button onClick={() => editor.chain().focus().mergeCells().run()} disabled={!editor.can().mergeCells()}>
        mergeCells
      </button>
      <button onClick={() => editor.chain().focus().splitCell().run()} disabled={!editor.can().splitCell()}>
        splitCell
      </button>
      <button onClick={() => editor.chain().focus().toggleHeaderColumn().run()} disabled={!editor.can().toggleHeaderColumn()}>
        toggleHeaderColumn
      </button>
      <button onClick={() => editor.chain().focus().toggleHeaderRow().run()} disabled={!editor.can().toggleHeaderRow()}>
        toggleHeaderRow
      </button>
      <button onClick={() => editor.chain().focus().toggleHeaderCell().run()} disabled={!editor.can().toggleHeaderCell()}>
        toggleHeaderCell
      </button>
      <button onClick={() => editor.chain().focus().mergeOrSplit().run()} disabled={!editor.can().mergeOrSplit()}>
        mergeOrSplit
      </button>
      <button onClick={() => editor.chain().focus().setCellAttribute('backgroundColor', '#FAF594').run()} disabled={!editor.can().setCellAttribute('backgroundColor', '#FAF594')}>
        setCellAttribute
      </button>
      <button onClick={() => editor.chain().focus().fixTables().run()} disabled={!editor.can().fixTables()}>
        fixTables
      </button>
      <button onClick={() => editor.chain().focus().goToNextCell().run()} disabled={!editor.can().goToNextCell()}>
        goToNextCell
      </button>
      <button onClick={() => editor.chain().focus().goToPreviousCell().run()} disabled={!editor.can().goToPreviousCell()}>
        goToPreviousCell
      </button>
      <br/> */}
      </div>

 
    </>
  )
}

//const content = `<p> Sample paragraph </p>`

const content =`<html>
    
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
  {font-family:"Cambria Math";
  panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
  {font-family:"Liberation Serif";}
@font-face
  {font-family:Cambria;
  panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
  {font-family:Tahoma;
  panose-1:2 11 6 4 3 5 4 4 2 4;}
@font-face
  {font-family:"Liberation Sans";}
@font-face
  {font-family:"Source Han Sans CN Regular";
  panose-1:0 0 0 0 0 0 0 0 0 0;}
@font-face
  {font-family:"Lohit Devanagari";
  panose-1:0 0 0 0 0 0 0 0 0 0;}
@font-face
  {font-family:"Noto Sans CJK SC";
  panose-1:0 0 0 0 0 0 0 0 0 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
  {margin:0in;
  line-height:115%;
  font-size:11.0pt;
  font-family:"Arial",sans-serif;}
p.TableContents, li.TableContents, div.TableContents
  {mso-style-name:"Table Contents";
  margin:0in;
  line-height:115%;
  font-size:11.0pt;
  font-family:"Arial",sans-serif;}
p.IntenseQuote, li.IntenseQuote, div.IntenseQuote
  {mso-style-name:IntenseQuote;
  margin-top:0in;
  margin-right:0in;
  margin-bottom:3.0pt;
  margin-left:0in;
  page-break-after:avoid;
  font-size:12.0pt;
  font-family:"Liberation Serif",serif;
  color:#4F81BD;
  font-weight:bold;
  font-style:italic;}
p.ListNumber, li.ListNumber, div.ListNumber
  {mso-style-name:ListNumber;
  margin-top:0in;
  margin-right:0in;
  margin-bottom:0in;
  margin-left:37.7pt;
  text-indent:-19.85pt;
  line-height:115%;
  font-size:11.0pt;
  font-family:"Arial",sans-serif;}
p.BoldUnderline, li.BoldUnderline, div.BoldUnderline
  {mso-style-name:BoldUnderline;
  margin:0in;
  line-height:115%;
  font-size:11.0pt;
  font-family:"Arial",sans-serif;
  font-weight:bold;
  text-decoration:underline;}
.MsoChpDefault
  {font-size:10.0pt;
  font-family:"Liberation Serif",serif;}
@page WordSection1
  {size:8.5in 11.0in;
  margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
  {page:WordSection1;}
 /* List Definitions */
 ol
  {margin-bottom:0in;}
ul
  {margin-bottom:0in;}
-->
</style>

</head>

<body lang=EN-US style='word-wrap:break-word'>

<div class=WordSection1>

<img width=328 height=92
id=image1.png src="https://mma.prnewswire.com/media/895957/safeBuilt_Logo.jpg"/>

<p class=MsoNormal><b><u><span lang=EN style='font-size:10.0pt;line-height:
115%'><span style='text-decoration:none'>&nbsp;</span></span></u></b></p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=624
 style='width:6.5in;border-collapse:collapse'>
 <tr>
  <td width=143 valign=top style='width:107.25pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
  <p class=TableContents><b><span lang=EN>Date:</span></b></p>
  </td>
  <td width=148 valign=top style='width:111.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
  <p class=TableContents><b><span lang=EN>June 14, 2021</span></b></p>
  </td>
  <td width=130 valign=top style='width:97.55pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
  <p class=TableContents><b><span lang=EN>Jurisdiction:</span></b></p>
  </td>
  <td width=203 valign=top style='width:152.15pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
  <p class=TableContents><b><span lang=EN>FIRESTONE</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=143 valign=top style='width:107.25pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
  <p class=TableContents><b><span lang=EN>Project:</span></b></p>
  </td>
  <td width=148 valign=top style='width:111.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
  <p class=TableContents><b><span lang=EN>-</span></b></p>
  </td>
  <td width=130 valign=top style='width:97.55pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
  <p class=TableContents><b><span lang=EN>Site Address:</span></b></p>
  </td>
  <td width=203 valign=top style='width:152.15pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
  <p class=TableContents><b><span lang=EN>-</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=143 valign=top style='width:107.25pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
  <p class=TableContents><b><span lang=EN>Permit#:</span></b></p>
  </td>
  <td width=148 valign=top style='width:111.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
  <p class=TableContents><b><span lang=EN>Yellow</span></b></p>
  </td>
  <td width=130 valign=top style='width:97.55pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
  <p class=TableContents><b><span lang=EN>Contact Person:</span></b></p>
  </td>
  <td width=203 valign=top style='width:152.15pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
  <p class=TableContents><b><span lang=EN>-</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=143 valign=top style='width:107.25pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
  <p class=TableContents><b><span lang=EN>Contact Phone:</span></b></p>
  </td>
  <td width=148 valign=top style='width:111.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
  <p class=TableContents><b><span lang=EN>-</span></b></p>
  </td>
  <td width=130 valign=top style='width:97.55pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
  <p class=TableContents><b><span lang=EN>Contact Email:</span></b></p>
  </td>
  <td width=203 valign=top style='width:152.15pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
  <p class=TableContents><b><span lang=EN>reviewer@safebuilt.com</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=143 valign=top style='width:107.25pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
  <p class=TableContents><b><span lang=EN>Construction Type</span></b></p>
  </td>
  <td width=148 valign=top style='width:111.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
  <p class=TableContents><a name="__DdeLink__566_1190928580"><b><span lang=EN>-</span></b></a></p>
  </td>
  <td width=130 valign=top style='width:97.55pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
  <p class=TableContents><b><span lang=EN>Occupancy Classification:</span></b></p>
  </td>
  <td width=203 valign=top style='width:152.15pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
  <p class=TableContents><b><span lang=EN>-</span></b></p>
  </td>
 </tr>
</table>

<p class=MsoNormal><span lang=EN style='color:white'><img width=2 height=4
id="Rectangle 2" src="non_compliance_Yellow_4158_files/image002.gif"></span></p>

<p class=MsoNormal style='text-align:justify'><u><span lang=EN
style='font-size:10.0pt;line-height:115%'>Please respond in writing to each
comment by marking the attached list or creating a response letter. Indicate
which plan sheet, detail, specification, or calculation shows the requested
information. </span></u></p>

<p class=MsoNormal style='text-align:justify'><u><span lang=EN
style='font-size:10.0pt;line-height:115%'>Responses such as “will comply with
code” are not adequate. Revised drawings must clearly show code compliance.</span></u></p>

<p class=MsoNormal><u><span lang=EN><span style='text-decoration:none'>&nbsp;</span></span></u></p>

<p class=MsoNormal><b><u><span lang=EN style='font-size:10.0pt;line-height:
115%'>A RESPONSE IN WRITING MUST BE INCLUDED WITH THE REVISED PLAN SUBMITTAL.</span></u></b></p>

<p class=BoldUnderline><span lang=EN><span style='text-decoration:none'>&nbsp;</span></span></p>

<p class=BoldUnderline><span lang=EN><br>
BUILDING DEPARTMENT COMMENTS:</span></p>

<p class=BoldUnderline><span lang=EN>All code references are for the 2018 IRC
UNO:</span></p>

<p class=ListNumber><span lang=EN>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN>Rule: VR17 Page 1: As per section R311.3, there shall
be a landing or floor on each side of each exterior door. The slope at exterior
landings shall not exceed 1/4 unit vertical in 12 units horizontal (2 percent)</span></p>

<p class=ListNumber><span lang=EN>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN>Rule: VR18 Page 1: As per R311.3.2, There shall be
a landing or floor on each side of each exterior door. Doors other than the
required egress door shall be provided with landings or floors not more than 7
3/4 inches (196 mm) below the top of the threshold. Exception: A top landing is
not required where a stairway of not more than two risers is located on the
exterior side of the door, provided that the door does not swing over the
stairway.</span></p>

<p class=ListNumber><span lang=EN>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN>Rule: VR11 Page 1: As per section R310.2.2, All
Egress window shall have a sill height of maximum 44 inch but, Information is
missing</span></p>

<p class=ListNumber><span lang=EN>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN>Rule: VR12 Page 1: The basement emergency egress
window(s) where located below grade shall have a sill height of not more than
44-inches above the finished floor and the window well serving the window shall
be not less than 9 square feet, with a horizontal projection and width of not
less than 36-inches. The area of the window well shall allow the emergency
escape window to fully open. Window wells with a vertical depth greater than
44-inches shall be provided with a permanently affixed ladder or steps usable
with the window fully opened. The ladder and or steps shall comply with the
provisions of section 310.2.3.1. Revise plans to show compliance.</span></p>

<p class=ListNumber><span lang=EN>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN>Rule: VR61 Page 1: Walls in shower compartments and
wall above bathtubs that have a wall or ceiling mounted shower head shall be finished
with a nonabsorbent surface. Such wall surfaces shall extend to a height of not
less than 6-feet above the floor per code sections R307.2 and P2710.1. Provide
notes and or details show compliance</span></p>

<p class=ListNumber><span lang=EN>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN>Rule: VR34 Page 1: Masonry and concrete walls shall
be dampproofed in accordance with section R406. Provide details or notes
verifying compliance. R406 Dampproofing.</span></p>

<p class=ListNumber><span lang=EN>7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN>Rule: VR36 Page 1: As per section R507.2.1, Lumber
Information should be present in the plan document</span></p>

<p class=ListNumber><span lang=EN>8.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN>Rule: VR35 Page 1: Floor assemblies that are not
required elsewhere in this code to be fire-resistance rated, shall be provided
with a minimum 1/2-inch (12.7 mm) gypsum wall-board membrane, or equivalent on
the underside of the floor framing member. Provide details or notes verifying
compliance on all floor-ceiling assemblies. R302.13 Fire protection of floors</span></p>

<p class=ListNumber><span lang=EN>9.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN>Rule: VR36 Page 1: As per section R507.2.1, Deck
load calculation should be present in the plan document</span></p>

<p class=ListNumber><span lang=EN>10.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR31 Page 1: As per R806, All Roofs should
have ventilation information to be provided.</span></p>

<p class=ListNumber><span lang=EN>11.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR9 Page 3: As per section R310, All bedrooms
shall have an emergency egress window,but none is found</span></p>

<p class=ListNumber><span lang=EN>12.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR23 Page 3: As per section R303.1, All
Bedrooms shall have a  minimum ventilation of 4%.But, it is found to be 0.0%</span></p>

<p class=ListNumber><span lang=EN>13.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR23 Page 3: As per section R303.1, All
Bedrooms shall have a minimum illumination of 8%.But, it is found to be 0.0% </span></p>

<p class=ListNumber><span lang=EN>14.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR20 Page 3: As per section R304.1, Bedroom
shall have a min area of 70sqft.But, it was found to be 54.77sqft</span></p>

<p class=ListNumber><span lang=EN>15.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR19 Page 3: As per section R311.6, Hallway
shall have a min width of 3ft. But it was found to be 2.42ft</span></p>

<p class=ListNumber><span lang=EN>16.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR23 Page 3: As per section R303.1, All
Bedrooms shall have a  minimum ventilation of 4%.But, it is found to be 0.0%</span></p>

<p class=ListNumber><span lang=EN>17.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR23 Page 3: As per section R303.1, All
Bedrooms shall have a minimum illumination of 8%.But, it is found to be 0.0% </span></p>

<p class=ListNumber><span lang=EN>18.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR23 Page 3: As per section R303.1, All
Bedrooms shall have a  minimum ventilation of 4%.But, it is found to be 0.0%</span></p>

<p class=ListNumber><span lang=EN>19.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR60 Page 3: As per section R307.1, There
shall be a clearance of not less than 21 inches (533 mm) (24 inches (610 mm) in
California) in front of a water closet, lavatory or bidet to any wall, fixture
or door. But, the clearence was found to be 19.2 inches</span></p>

<p class=ListNumber><span lang=EN>20.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR23 Page 3: As per section R303.1, All
Bedrooms shall have a minimum illumination of 8%.But, it is found to be 0.0% </span></p>

<p class=ListNumber><span lang=EN>21.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR3 Page 3: Distance between shower area and
window shall be 5 feet as per R308.4.3. But, distance was found to be
1.0218732066029792 feet</span></p>

<p class=ListNumber><span lang=EN>22.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR3 Page 3: Distance between bathtub and
window shall be 5 feet as per R308.4.3. But, distance was found to be 0.0 feet</span></p>

<p class=ListNumber><span lang=EN>23.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR3 Page 3: Distance between shower area and
window shall be 5 feet as per R308.4.3. But, distance was found to be
0.7849029315907231 feet</span></p>

<p class=ListNumber><span lang=EN>24.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR58 Page 3: As per section R307.1, A water
closet, lavatory, or bidet shall not be set closer than 15 inches (381 mm) to
any SIDE WALL, PARTITION OR VANITY or closer than 30 inches (762 mm)
center-to-center between adjacent fixtures. But, the lavatory/water closet was
found to be having a side clearence of only 13.61 inches.</span></p>

<p class=ListNumber><span lang=EN>25.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR23 Page 3: As per section R303.1, All
Bedrooms shall have a  minimum ventilation of 4%.But, it is found to be 0.0%</span></p>

<p class=ListNumber><span lang=EN>26.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR23 Page 3: As per section R303.1, All
Bedrooms shall have a minimum illumination of 8%.But, it is found to be 0.0% </span></p>

<p class=ListNumber><span lang=EN>27.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR60 Page 3: As per section R307.1, There
shall be a clearance of not less than 21 inches (533 mm) (24 inches (610 mm) in
California) in front of a water closet, lavatory or bidet to any wall, fixture
or door. But, the clearence was found to be 18.79 inches</span></p>

<p class=ListNumber><span lang=EN>28.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR19 Page 3: As per section R311.6, Hallway
shall have a min width of 3ft. But it was found to be 1.53ft</span></p>

<p class=ListNumber><span lang=EN>29.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR19 Page 3: As per section R311.6, Hallway
shall have a min width of 3ft. But it was found to be 2.82ft</span></p>

<p class=ListNumber><span lang=EN>30.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR19 Page 3: As per section R311.6, Hallway
shall have a min width of 3ft. But it was found to be 0.05ft</span></p>

<p class=ListNumber><span lang=EN>31.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR44 Page 4: As per R311.7.1, All stairways
shall have a minimum width of 36 inches . But , its found to be 34.99 inches</span></p>

<p class=BoldUnderline><span lang=EN><br>
ELECTRICAL DEPARTMENT COMMENTS:</span></p>

<p class=BoldUnderline><span lang=EN>All code references are for the 2018 IRC
UNO:</span></p>

<p class=ListNumber><span lang=EN>32.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR13 Page 1: As per section R210.8, GFCI
notes shall have present in the general notes in electrical plan, but GFCI
notes is not found in general notes in electrical plan</span></p>

<p class=ListNumber><span lang=EN>33.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR14 Page 1: As per section E3902.16, AFCI
notes shall have present in the general notes in electrical plan, but AFCI
notes is not found in general notes in electrical plan</span></p>

<p class=ListNumber><span lang=EN>34.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR39 Page 1: As per section R314.6, Smoke
alarms shall be permitted to be battery operated when installed in buildings
without commercial power</span></p>

<p class=BoldUnderline><span lang=EN><br>
MECHANICAL DEPARTMENT COMMENTS:</span></p>

<p class=BoldUnderline><span lang=EN>All code references are for the 2018 IRC
UNO:</span></p>

<p class=ListNumber><span lang=EN>35.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR30 Page 1: As per section R303.6, Openings
that terminate outdoors shall be protected with corrosion-resistant screens,
louvers or grilles having an opening size of not less than 1/4 inch (6 mm) and
a maximum opening size of 1/2 inch (13 mm)</span></p>

<p class=ListNumber><span lang=EN>36.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR29 Page 1: As per section R303.5.1, 10-foot
(3048 mm) separation is not required where the intake opening is located 3 feet
(914 mm) or greater below the contaminant source</span></p>

<p class=ListNumber><span lang=EN>37.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR29 Page 1: As per section R303.5.1, Intake
openings shall be located not less than 10 feet (3048 mm) from any hazardous or
noxious contaminant</span></p>

<p class=BoldUnderline><span lang=EN><br>
GENERAL DEPARTMENT COMMENTS:</span></p>

<p class=BoldUnderline><span lang=EN>All code references are for the 2018 IRC
UNO:</span></p>

<p class=ListNumber><span lang=EN>38.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN>Rule: VR2 Page 1: As per section R308.4.2, Safety
/Tempered glazing required adjacent to doors-24in</span></p>

</div>

</body>

</html>
`;


// export default () => {
//   const editor = useEditor({
//     extensions: [
//       StarterKit,
//       Table.configure({
//         resizable: true,
//       }),
//       TableRow,
//       TableHeader,
//       // Default TableCell
//       // TableCell,
//       // Custom TableCell with backgroundColor attribute
//       CustomTableCell,
//       Document,
//       Paragraph,
//       Text,
//       Image,
//       Dropcursor,
//       CustomExtension,
//       Collaboration.configure({
//         document: ydoc,
//       }),
//     ],
//     content: '<p>Hello World</p>'
//   })

  
  

  
//   return (
//     <div>
//       <MenuBar editor={editor} />
//       <EditorContent editor={editor}  />
//     </div>
//   )
// }

export default () => {
  const editor = useEditor({
    extensions: [
      StarterKit,
      Table.configure({
                //resizable: true,
              }),
              TableRow,
              TableHeader,
              // Default TableCell
              // TableCell,
              // Custom TableCell with backgroundColor attribute
              CustomTableCell,
              Document,
              Paragraph,
              Text,
              Image,
              Dropcursor,
              CustomExtension,
              TextAlign.configure({
                types: ['heading', 'paragraph'],
              }),
              Highlight
              // Collaboration.configure({
              //   document: ydoc,
              // }),
           
    
    
    ],
    content: content
  });

  

  return (
    <>
      <MenuBar editor={editor}/>
      <EditorContent editor={editor} />
    </>
  );
};


